// Initialize app
var myApp = new Framework7();


// If we need to use custom DOM library, let's save it to $$ variable:
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we want to use dynamic navbar, we need to enable it for this view:
    dynamicNavbar: true
});

// Handle Cordova Device Ready Event
$$(document).on('deviceready', function() {
    console.log("Device is ready!");
});


// Now we need to run the code that will be executed only for About page.

// Option 1. Using page callback for page (for "about" page in this case) (recommended way):


/** LOCAL STORAGE SAMPLE **/
myApp.onPageInit('storageSample', function (page) {
    
    //using F7 Dom Function, refer to https://framework7.io/docs/dom.html
    
    //load data to fields on page load
    //get fields in local storage
    var storage = window.localStorage;
    var name = storage.getItem("name");
    var email = storage.getItem("email");
    //set form fiels by id
    $$('#name').val(name);
    $$('#email').val(email);


    //save button event
    $$('.save').on('click', function (e) {
        //get fields values by id
        var name = $$('#name').val();
        var email = $$('#email').val();

        //save fields in local storage
        var storage = window.localStorage;
        storage.setItem("name", name);
        storage.setItem("email", email);
    });


});



/** WEB SQL SAMPLE **/
myApp.onPageInit('websqlsample', function (page) {

    //open or create database
    window.__webSqlDebugModeOn = true;

    var db = window.openDatabase("websqlsample", "1.0", "Test DB", 1000000);

    db.transaction(function (tx) {
        //insert records
        tx.executeSql("CREATE TABLE IF NOT EXISTS note (name text, data text)");

    });

    db.transaction(function (tx) {
            //insert records
            tx.executeSql("INSERT INTO note (name, data) VALUES ('name1','teste1')");
            tx.executeSql("INSERT INTO note (name, data) VALUES ('name2','teste2')");
            
        });
    
    // select record
    var sqlStatement = "SELECT * FROM note";
    var valueArray = [];

    db.transaction(function (tx) {
        tx.executeSql(sqlStatement, valueArray, function (tx, result) {
            //success
            console.log('result lenght: ' + result.rows.length);

            for (i=0; i< result.rows.length; i++)
            {
                alert(result.rows.item(i).name);
            }
            
        }, function (error) {
            console.log(error);
            alert("error:" + error);
        });
    });

});



/** AJAX SAMPLE - Virtual List **/
myApp.onPageInit('ajaxSample', function (page) {
    
    $$.ajax({
        url: 'http://www.lovetodance.pt/dados.json',
        type: 'POST',
       // data: { acao : acao },
        datatype: 'json',
        success:  function(data) {

            var myList = myApp.virtualList('.list-block.virtual-list.lista', {
                // Array with items data
                
                items: JSON.parse(data),
                // Template 7 template to render each item
                template: `<li class="item-content">{{date}} - {{name}}</li>`,
                            height: 44,
                            rowsBefore : 5,
                            rowsAfter : 5
            });

        }
    });



});


/** AJAX SAMPLE - Generated **/
myApp.onPageInit('ajaxSample2', function (page) {
    
    $$.ajax({
        url: 'http://www.lovetodance.pt/dados.json',
        type: 'POST',
       // data: { acao : acao },
        datatype: 'json',
        success:  function(data) {

            var itemsHtml = '';
            var items = JSON.parse(data);
            items.forEach(function(element) {
                var item =  '<div>' + element.date  + ' - ' + element.name  + '</div>';
                itemsHtml += item;
            }, this);

            $$('.manual-list').html(itemsHtml);

        }
    });



});

